package ru.tsc.vinokurov.tm.command.system;

import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractSystemCommand {

    public static final String NAME = "arguments";

    public static final String DESCRIPTION = "Show argument list.";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getArgumentCommands();
        commands.stream()
                .map(AbstractCommand::getArgument)
                .forEach(System.out::println);
    }

}

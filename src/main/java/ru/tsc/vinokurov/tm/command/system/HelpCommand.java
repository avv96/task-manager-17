package ru.tsc.vinokurov.tm.command.system;

import ru.tsc.vinokurov.tm.api.model.ICommand;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String DESCRIPTION = "Show terminal commands.";

    public static final String ARGUMENT = "-h";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}

package ru.tsc.vinokurov.tm.util;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static Date nextDate() {
        return DateUtil.toDate(SCANNER.nextLine());
    }
    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        return Integer.parseInt(value);
    }
}

package ru.tsc.vinokurov.tm.enumerated;

import ru.tsc.vinokurov.tm.api.model.IWBS;
import ru.tsc.vinokurov.tm.comparator.CreatedComparator;
import ru.tsc.vinokurov.tm.comparator.DateBeginComparator;
import ru.tsc.vinokurov.tm.comparator.NameComparator;
import ru.tsc.vinokurov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    private String displayName;

    private final Comparator comparator;

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort: values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
